export interface IUser {
  id: string;
  uid: string;
  email: string,
  displayName: string,
  photoURL: string,
  emailVerified: string,
}
