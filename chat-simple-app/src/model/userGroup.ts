export interface IUserGroup {
  id: string;
  uidFromTo: any[];
  groupName: string;
  type: string;
}
