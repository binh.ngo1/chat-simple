export interface IMessage {
  id: string;
  idUserGroup: string;
  uidFrom: string;
  message: string,
  imageMessage: string,
  created_date: Date;
}
