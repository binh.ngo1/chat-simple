import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/compat/firestore';
import { map, Observable } from 'rxjs';



@Injectable({
  providedIn: 'root'
})
export class BaseFireStoreService<T extends IBaseStoreCollection> {
  collectionName = '';
  collection!: AngularFirestoreCollection<T>;
  constructor(
    private db: AngularFirestore
  ) {
  }

  init(name: string) {
    this.collectionName = name;
    this.collection = this.db.collection(this.collectionName);
  }

  getAll() {
    return this.collection.snapshotChanges().pipe(
      map(changes => changes.map(c => {
        const item = c.payload.doc.data();
        item.id = c.payload.doc.id;
        return item;
      }))
    );
  }

  get(id: string): Observable<T | null> {
    return this.collection.doc(id).snapshotChanges().pipe(
      map(changes => (changes.payload.data() ? { id: changes.payload.id, ...changes.payload.data() } as T : null))
    )
  }

  getRef(id: string) {
    return this.collection.doc(id).ref;
  }

  create(data: T) {
    const saveData = { ...data } as any;
    delete saveData.id;
    return this.collection.add({ ...saveData });
  }
  update(id: string, data: any): Promise<void> {
    const saveData = { ...data } as any;
    delete saveData.id;
    return this.collection.doc(id).update(saveData);
  }
  delete(id: string): Promise<void> {
    return this.collection.doc(id).delete();
  }
}

export interface IBaseStoreCollection {
  id: string;
  [prop: string]: any;
}
