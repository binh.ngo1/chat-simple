import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { BaseFireStoreService } from './base-firestore.service';

@Injectable({
  providedIn: 'root'
})
export class UserService extends BaseFireStoreService<any> {

  constructor(db: AngularFirestore, private af: AngularFirestore) {
    super(db);
    this.init('users');
  }


  getUserByUid(uid: any) {
    return this.af.collection('users', ref => ref.where('uid', '==', uid)).valueChanges();
  }
}
