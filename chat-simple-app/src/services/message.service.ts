import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { BaseFireStoreService } from './base-firestore.service';
import { IMessage } from 'src/model/message';

@Injectable({
  providedIn: 'root'
})
export class MessageService extends BaseFireStoreService<IMessage> {
  constructor(db: AngularFirestore, private af: AngularFirestore) {
    super(db);
    this.init('messages');
  }

  getMessByUidFromTo(idUserGroup: string) {
    return this.af.collection('messages', ref => (ref.where('idUserGroup', '==', idUserGroup))).valueChanges();
  }
}
