import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { IUserGroup } from 'src/model/userGroup';
import { BaseFireStoreService } from './base-firestore.service';
import { map, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserGroupService extends BaseFireStoreService<IUserGroup> {

  constructor(db: AngularFirestore, private af: AngularFirestore) {
    super(db);
    this.init('userGroup');
  }

  getListUserFromToByUserFrom(uidFrom: string) {
    return this.af.collection('userGroup', ref => ref.where('uidFromTo', 'array-contains', uidFrom)).snapshotChanges().pipe(
      map((changes: any) => changes.map((c: any) => {
        const item = c.payload.doc.data();
        item.id = c.payload.doc.id;
        return item;
      }))
    );
  }
  getUserFromToByUserFromPvp(uidFrom: string, uidTo: string) {
    // return this.af.collection('userGroup', ref => (ref.where('uidFromTo', 'in', [uidFrom, uidTo]))).snapshotChanges().pipe(
    //   map((changes: any) => changes.map((c: any) => {
    //     const item = c.payload.doc.data();
    //     item.id = c.payload.doc.id;
    //     return item;
    //   }))
    // );
    return this.af.collection('userGroup', ref => (ref.where('uidFromTo', '==', [uidFrom, uidTo]), ref.where('uidFromTo', '==', [uidTo, uidFrom]))).valueChanges();

  }
}
