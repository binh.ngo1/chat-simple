// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyDwootVZRHKx36fcgBWUO182DswqNGdc2w",
    authDomain: "chat-single-25482.firebaseapp.com",
    projectId: "chat-single-25482",
    storageBucket: "chat-single-25482.appspot.com",
    messagingSenderId: "916104821797",
    appId: "1:916104821797:web:7f2d5bba239b9c1aefd3cc",
    measurementId: "G-TJRKE7F1DT"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
