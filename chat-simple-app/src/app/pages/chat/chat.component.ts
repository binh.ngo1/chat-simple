import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject, takeUntil } from 'rxjs';
import { IUser } from 'src/model/user';
import { AuthService } from 'src/services/auth.service';
import { MessageService } from 'src/services/message.service';
import { UserService } from 'src/services/user.service';
import { UserGroupService } from 'src/services/user-group.service';
import { ToastService } from 'src/services/toast.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss']
})
export class ChatComponent implements OnInit, OnDestroy {

  $destroy = new Subject();
  UserFrom!: IUser;
  UserTo: any;
  users: any;
  mess: string = '';
  arrMess: any = [];
  uidFrom: string = '';
  uidTo: string = '';
  idUserGroup: string = '';
  status: string = '';
  userGroup: any;

  uidSelect: any = '';
  displayNameSelect: any = '';
  displayNameGroup: any = '';

  constructor(
    private activatedRoute: ActivatedRoute,
    public router: Router,
    private authService: AuthService,
    private messageService: MessageService,
    private userService: UserService,
    private userGroupService: UserGroupService,
    public toastService: ToastService,
    private modalService: NgbModal
  ) {
  }

  ngOnInit() {

    this.userService.getAll()
      .pipe(
        takeUntil(this.$destroy)
      )
      .subscribe((data: any) => {
        this.users = data;
      });

    const user = this.authService.getUser();

    if (user) {
      this.UserFrom = user;

      this.uidTo = this.activatedRoute.snapshot.params['id'];
      this.uidFrom = this.UserFrom.uid;

      this.status = this.activatedRoute.snapshot.params['status'];

      this.userGroupService.get(this.uidTo).subscribe((data: any) => {
        if (data) {
          this.userGroup = data;
          if (!data.uidFromTo.includes(this.UserFrom.uid)) {
            this.router.navigate(['dashboard']);
          }
        } else {
          // const ref = this.userGroupService.create({ id: '', groupName: this.uidFrom, uidFromTo: [this.authService.getUser().uid], type: 'group' });
        }
      })

      if (this.status == 'pvp') {
        // this.userGroupService.getUserFromToByUserFromPvp(this.uidFrom, this.uidTo).subscribe((data: any) => {
        //   if (data.length > 0) {
        //     this.idUserGroup = data[0].id;
        //     this.messageService.getMessByUidFromTo(data[0].id).pipe(
        //       takeUntil(this.$destroy)
        //     ).subscribe((data: any) => {
        //       this.arrMess = data;
        //       this.arrMess = this.arrMess.sort((a: any, b: any) => a.created_date.seconds - b.created_date.seconds);
        //     })
        //   } else {
        //     this.userGroupService.create({ id: '', groupName: this.uidFrom, uidFromTo: [this.authService.getUser().uid, this.uidTo], type: 'pvp' }).then((data) => {
        //       this.idUserGroup = data.id;
        //     })
        //   }
        // })
      } else if (this.status == 'group') {
        this.idUserGroup = this.activatedRoute.snapshot.params['id'];
        this.messageService.getMessByUidFromTo(this.idUserGroup).pipe(
          takeUntil(this.$destroy)
        ).subscribe((data: any) => {
          this.arrMess = data;
          this.arrMess = this.arrMess.sort((a: any, b: any) => a.created_date.seconds - b.created_date.seconds);
        })
      } else {
        this.router.navigate(['dashboard']);
      }
    }
  }

  sendMessage() {
    if (this.mess != '' && this.idUserGroup != '') {
      const datetime = new Date();
      this.messageService.create({ id: '', idUserGroup: this.idUserGroup, uidFrom: this.uidFrom, imageMessage: '', message: this.mess, created_date: datetime });
      this.mess = '';
    }
  }

  addUserGroup() {

    // chat pvp add >2 user thì thành group

    if (this.userGroup.uidFromTo.includes(this.uidSelect)) {
      this.toastService.show(this.displayNameSelect + ' đã có trong nhóm', { classname: 'bg-danger text-light', delay: 2000 });
    } else {
      const isUser = this.users.find((element: any) => element.uid == this.uidSelect);
      if (isUser) {
        this.userGroup.uidFromTo.push(this.uidSelect);
        this.userGroupService.update(this.userGroup.id, this.userGroup);
        this.uidSelect = '';
        this.toastService.show('Bạn đã thêm ' + this.displayNameSelect + ' vào nhóm', { classname: 'bg-success text-light', delay: 2000 });
      } else {
        this.toastService.show('Không tìm thấy người dùng tương ứng', { classname: 'bg-danger text-light', delay: 2000 });
      }
    }
  }

  addValue(item: any) {
    this.uidSelect = item.uid;
    this.displayNameSelect = item.displayName;
  }

  ngOnDestroy(): void {
    this.toastService.clear();
  }

  getUrlAvt(uid: any) {
    const user = this.users.find((element: any) => element.uid == uid);
    return user.photoURL ? user.photoURL : '';
  }

  getDisplayName(uid: any) {
    const user = this.users.find((element: any) => element.uid == uid);
    return user.displayName ? user.displayName : '';
  }

  open(content: any, groupName: any) {
    this.displayNameGroup = groupName;
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.userGroup.groupName = this.displayNameGroup;
      this.userGroupService.update(this.userGroup.id, this.userGroup);
      this.displayNameGroup = '';
    }, (reason) => {
      this.displayNameGroup = '';
    });
  }
}
