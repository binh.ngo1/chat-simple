import { Component, OnInit } from '@angular/core';
import { Subject, takeUntil } from 'rxjs';
import { AuthService } from 'src/services/auth.service';
import { UserGroupService } from 'src/services/user-group.service';
import { UserService } from 'src/services/user.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  users: any;
  listUserFromTo: any;
  groupName: string = '';
  $destroy = new Subject();

  constructor(
    public authService: AuthService,
    private userService: UserService,
    private userGroupService: UserGroupService
  ) { }

  ngOnInit(): void {
    this.userService.getAll()
      .pipe(
        takeUntil(this.$destroy)
      )
      .subscribe((data: any) => {
        this.users = data;
      });

    this.userGroupService.getListUserFromToByUserFrom(this.authService.getUser().uid).subscribe((data: any) => {
      this.listUserFromTo = data;
    })
  }

  addGroup() {
    this.userGroupService.create({ id: '', groupName: this.groupName, uidFromTo: [this.authService.getUser().uid], type: 'group' });
    this.groupName = '';
  }

}
